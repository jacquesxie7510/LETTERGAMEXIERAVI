package fr.esiea.XIE.RAVI.Joueurs;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.esiea.XIE.RAVI.Plateau.IActionPlayer;
import fr.esiea.XIE.RAVI.Plateau.LettresPlateau;
import fr.esiea.XIE.RAVI.PotCommun.Bag;
import fr.esiea.XIE.RAVI.Words.IWord;
import fr.esiea.XIE.RAVI.Words.Word;


public class Player implements IActionPlayer,IWord{
	
	private Bag LettresPlayer;
	private List<Word> MesMots;
	private String Name;
	
	public Player(LettresPlateau pot,String Nom){
		this.LettresPlayer= new Bag(pot);
		this.setMesMots(new ArrayList<Word>());
		for (int i=0;i< 10;i++){
			Word lala=new Word();
			getMesMots().add(lala);
		}
		this.Name=Nom;
	}
	
	//setter & getter du nom du player
	public String getName() {
		return Name;
	}
	
	public void setName(String name) {
		Name = name;
	}
	
	//ajouter une lettre au potcommun
	
	public void Piocher(){
		LettresPlayer.getNextLetter(getRandomPlayer());
	}
	
	

	@Override
	public int ActualWord() {
		int i=0;
		while(getMesMots().get(i).getWordPlayer()!=("") && i<getMesMots().size()){
			i++;
		};
		
		return i;
	}
	
	
	//fonction de word
	@Override
	public void DeleteWord() {
		// TODO Auto-generated method stub
		
	}
	
	//suppression d'un mot de la liste du player
	@Override
	public void DeleteWordPlayer(int PlaceWord) {
		Word ThisWord=getMesMots().get(PlaceWord);
		ThisWord.DeleteWord();
		// TODO Auto-generated method stub
		
	}
	
	//valeur random pour determiner la lettre en int
	/*
	 * alphabet
	 * A b c d
	 * E f g h
	 * I j k l m n
	 * O p q r s t
	 * U v w x
	 * Y z
	 * 
	 * on cherche la voyelle ou consonne avec typelettre aleatoire
	 * puis il y un random sur 6 catégories 
	 * si consonne il a un random pour savoir laquel des listes au dessus 
	 * 
	 * */

	@Override
	public int getRandomPlayer() {
		Random r = new Random();
		return r.nextInt(26) ;

	}
	

	public List<Word> getMesMots() {
		return MesMots;
	}

	public void setMesMots(List<Word> mesMots) {
		MesMots = mesMots;
	}

}
