package fr.esiea.XIE.RAVI.Words;



public interface IWord {
	
	int ActualWord();
	
	void DeleteWord();
	
	void DeleteWordPlayer(int PlaceWord);


}
