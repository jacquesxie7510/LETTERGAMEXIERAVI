package fr.esiea.XIE.RAVI.Words;

public interface IWordWrite {
	void ChangeWordToListChar();
	boolean MotVerifier();
	boolean dicoVerifier(String MotTest);
	boolean NouveauWord();
	void NoAccent();

}
